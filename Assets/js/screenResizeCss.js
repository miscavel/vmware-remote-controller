$(window).on('load resize', function() 
{
	var win = $(this);
	if (win.width() < 768) 
	{
		$('.right-btn-group').removeClass('btn-group-vertical');
		$('.right-btn-group').addClass('btn-group');
	}
	else
	{
		$('.right-btn-group').removeClass('btn-group');
		$('.right-btn-group').addClass('btn-group-vertical');		
	}
});