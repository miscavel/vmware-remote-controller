var vmInfo;
var vmState;
var vncState;

$("document").ready(function(){

    $("#file").change(function() 
    {
        alert('changed!');
    });
});

$.getJSON("Assets/js/vmInfo.json", function(data) 
{
	vmInfo = data;
	LoadVMListToSelection();
	LoadMenuToSelection();
	VNCOnChange(document.getElementById('vmSelection').selectedIndex);
})

function ChangeFileText()
{
	path = document.getElementById('file').value;
	path = path.split('\\');
	path = path[path.length - 1];
	document.getElementById('upload-file-info').innerHTML = path;
}

function FileDownload()
{
	index = document.getElementById('vmSelection').selectedIndex;
	dirSelection = document.getElementById('dirName');
	dirName = dirSelection.options[dirSelection.selectedIndex].value;
	fileName = document.getElementById('fileName').value;
	action = vmInfo["menu"][document.getElementById('menuSelection').selectedIndex]["action"];
	vmAjax = $.get("Assets/php/vmUploadFile.php",
	{
		id:vmInfo["vm"][index]["id"], 
		path:vmInfo["vm"][index]["path"], 
		username:vmInfo["vm"][index]["username"],
		password:vmInfo["vm"][index]["password"],
		vncPort:vmInfo["vm"][index]["vncPort"],
		vmAction:action,
		param_1:dirName,
		param_2:fileName,
		param_3:vmInfo["downloadDir"],
		param_4:fileName
	}
	,function(result)
	{
		result = JSON.parse(result);
		console.log(result);
		
		document.getElementById('srcPath').value = vmInfo["downloadDir"];
		document.getElementById('srcFile').value = fileName;
		document.getElementById('downloadForm').submit();
	});
}

function FileUpload()
{
	var formData = new FormData();
	formData.append('file', $('#file')[0].files[0]);

	$.ajax({
	       url : 'Assets/php/uploadLocal.php',
	       type : 'POST',
	       data : formData,
	       processData: false,  // tell jQuery not to process the data
	       contentType: false,  // tell jQuery not to set contentType
	       success : function(data) 
	       {
           		srcFile = data;
				index = document.getElementById('vmSelection').selectedIndex;
				dirSelection = document.getElementById('dirName');
				dirName = dirSelection.options[dirSelection.selectedIndex].value;
				fileName = document.getElementById('fileName').value;
				action = vmInfo["menu"][document.getElementById('menuSelection').selectedIndex]["action"];
				vmAjax = $.get("Assets/php/vmUploadFile.php",
				{
					id:vmInfo["vm"][index]["id"], 
					path:vmInfo["vm"][index]["path"], 
					username:vmInfo["vm"][index]["username"],
					password:vmInfo["vm"][index]["password"],
					vncPort:vmInfo["vm"][index]["vncPort"],
					vmAction:action,
					param_1:vmInfo["uploadDir"],
					param_2:srcFile,
					param_3:dirName,
					param_4:fileName
				}
				,function(result)
				{
					result = JSON.parse(result);
					console.log(result);
				});
	      	}
	});
}

function GetVMState(index)
{
	vmAjax = $.get("Assets/php/getVMState.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:"6"
					}
					,function(result)
					{
						result = JSON.parse(result);
						console.log(result);
						vmState = result[3];
						document.getElementById("vmStateLabel").innerHTML = "VM State : " + result[4];
						if (vmState == "2")
						{
							document.getElementById("monitorBG").src = vmInfo["noConnectionUrl"];
						}
						GetVNCState(index);
					});
	return vmAjax;
}

function GetVNCState(index)
{
	vmAjax = $.get("Assets/php/getVNCState.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:"7"
					}
					,function(result)
					{
						result = JSON.parse(result);
						console.log(result);
						vncState = result[3];
						if (!(vncState == "On") && !(vncState == "Off"))
						{
							vncState = "Off";
						}
						document.getElementById("vncStateLabel").innerHTML = "VNC State : " + vncState;
					});
	return vmAjax;
}

function LoadMenuToSelection()
{
	var menuSelection = $("select[id=menuSelection]");
	var str = "";
	for(var i = 0; i < vmInfo["menu"].length; i++)
	{
		str += "<option>" + vmInfo["menu"][i]["id"] + "</option>";
	}
	menuSelection.html(str);
}

function LoadVMListToSelection()
{
	var vmSelection = $("select[id=vmSelection]");
	var str = "";
	for(var i = 0; i < vmInfo["vm"].length; i++)
	{
		str += "<option>" + vmInfo["vm"][i]["id"] + "</option>";
	}
	vmSelection.html(str);
}

function MenuOnChange(index)
{
	var menuForm = $("div[id=menuForm]");
	var str = "";
	for(var i = 0; i < vmInfo["menu"][index]["requires"].length; i++)
	{
		str += vmInfo["menu"][index]["requires"][i];
	}
	menuForm.html(str);
	if (vmInfo["menu"][index]["action"] == "13")
	{
		PullSnapshots(vmInfo["menu"][index]["action"]);
	}
}

function PullSnapshots(action)
{
	index = document.getElementById('vmSelection').selectedIndex;
	ajaxcall = $.get("Assets/php/pullSnapshot.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:action
					},
					function(result)
					{
						result = JSON.parse(result);
						var selectElement = $("select[id=snapshotSelect]");
						var str = "";
						for(var i = 2; i < result.length; i++)
						{
							str += "<option value='" + (i - 2) + "'>" + result[i] + "</option>";
						}
						selectElement.html(str);
						GetSnapshotDescription();
					});
}

function GetSnapshotDescription()
{
	index = document.getElementById('vmSelection').selectedIndex;
	snapshotID = document.getElementById('snapshotSelect').selectedIndex;
	ajaxcall = $.get("Assets/php/getSnapshotDescription.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:"14",
						snapshotID:snapshotID
					},
					function(result)
					{
						result = JSON.parse(result);
						document.getElementById('snapshotDescription').value = result[2];
					});
}

function RunProgram()
{
	index = document.getElementById('vmSelection').selectedIndex;
	dirSelection = document.getElementById('dirName');
	dirName = dirSelection.options[dirSelection.selectedIndex].value;
	appName = document.getElementById('appName').value;
	action = vmInfo["menu"][document.getElementById('menuSelection').selectedIndex]["action"];
	vmAjax = $.get("Assets/php/vmNoFileCommand.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:action,
						param_1:dirName,
						param_2:appName
					}
					,function(result)
					{
						result = JSON.parse(result);
						console.log(result);
					});
	return vmAjax;
}

function ScreenShot(index)
{
	index = document.getElementById('vmSelection').selectedIndex;
	action = vmInfo["menu"][document.getElementById('menuSelection').selectedIndex]["action"];
	vmAjax = $.get("Assets/php/vmNoFileCommand.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:action,
						param_1:vmInfo["screenshotDir"],
						param_2:vmInfo["screenshotFile"]
					}
					,function(result)
					{
						result = JSON.parse(result);
						console.log(result);

						document.getElementById('srcPath').value = vmInfo["screenshotDir"];
						document.getElementById('srcFile').value = vmInfo["screenshotFile"];
						document.getElementById('downloadForm').submit();

						document.getElementById('screenshot').src = vmInfo["homeDir"] + vmInfo["screenshotDir"] + vmInfo["screenshotFile"] + "?" + Date.now();
					});
	return vmAjax;
}

function SetVNCSource(index)
{
	document.getElementById("monitorBG").src = vmInfo["noConnectionUrl"];
	$.when(GetVMState(index)).done(function() 
	{
		if(vmState == "8")
		{
			$.when(StopVNCSource(index)).done(function() 
			{
				vncAjax = $.get("Assets/php/getGuestIP.php",
							{
								id:vmInfo["vm"][index]["id"], 
								path:vmInfo["vm"][index]["path"], 
								username:vmInfo["vm"][index]["username"],
								password:vmInfo["vm"][index]["password"],
								vncPort:vmInfo["vm"][index]["vncPort"],
								vmAction:"0"
							}
							,function(result)
							{
								result = JSON.parse(result);
								console.log(result);

								var ip = result[2];
								var vncUrl = vmInfo["vncUrl"];
								vncUrl = vncUrl.replace(/ip/g, ip);

								$.when(StartVNCSource(index, vncUrl)).done(function() 
								{
									setTimeout(function()
									{
										$.when(GetVNCState(index)).done(function() 
										{
											if (vncState == "Off")
											{
												SetVNCSource(index);
											}
											else if (vncState == "On")
											{
												console.log(vncUrl);
												document.getElementById("monitorBG").src = vncUrl;
											}
											else
											{
												SetVNCSource(index);
											}
										});
									}, 2000);
								});
							});
			});
		}
		else
		{
			alert('VM needs to be Powered On');
		}
	});
}

function SnapshotCreate()
{
	index = document.getElementById('vmSelection').selectedIndex;
	snapshotName = document.getElementById('newSnapshotName').value;
	snapshotDescription = document.getElementById('newSnapshotDescription').value;
	ajaxcall = $.get("Assets/php/vmNoFileCommand.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:"15",
						param_1:snapshotName,
						param_2:snapshotDescription
					},
					function(result)
					{
						result = JSON.parse(result);
						console.log(result);
						PullSnapshots();
					});
}

function SnapshotRestore()
{
	index = document.getElementById('vmSelection').selectedIndex;
	snapshotID = document.getElementById('snapshotSelect').selectedIndex;
	ajaxcall = $.get("Assets/php/snapshotRestore.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:"16",
						snapshotID:snapshotID
					},
					function(result)
					{
						result = JSON.parse(result);
					});
}

function StartVNCSource(index, vncUrl)
{
	document.getElementById("vncStateLabel").innerHTML = "VNC State : Loading";
	vncAjax = $.get("Assets/php/startVNCServer.php",
				  	{	
				  		id:vmInfo["vm"][index]["id"], 
					  	path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:"1"
			  		}
				  	,function(result)
				  	{
					  	result = JSON.parse(result);
						console.log(result);
						//console.log(vncUrl);
						//document.getElementById("monitorBG").src = vncUrl;
				  	});
	return vncAjax;
}

function StopVNCSource(index)
{
	document.getElementById("vncStateLabel").innerHTML = "VNC State : Loading";
	vncAjax = $.get("Assets/php/stopVNCServer.php",
			{
				id:vmInfo["vm"][index]["id"], 
				path:vmInfo["vm"][index]["path"], 
				username:vmInfo["vm"][index]["username"],
				password:vmInfo["vm"][index]["password"],
				vncPort:vmInfo["vm"][index]["vncPort"],
				vmAction:"2"
			}
			,function(result)
			{
				result = JSON.parse(result);
				console.log(result);
				GetVNCState(index);
			});
	return vncAjax;
}

function VMPowerCall(index, action)
{
	$.when(GetVMState(index)).done(function() 
	{
		vmAjax = $.get("Assets/php/vmPowerOption.php",
					{
						id:vmInfo["vm"][index]["id"], 
						path:vmInfo["vm"][index]["path"], 
						username:vmInfo["vm"][index]["username"],
						password:vmInfo["vm"][index]["password"],
						vncPort:vmInfo["vm"][index]["vncPort"],
						vmAction:action
					}
					,function(result)
					{
						result = JSON.parse(result);
						console.log(result);
						if(action == "3")
						{
							SetVNCSource(index);
						}
						$.when(GetVMState(index)).done(function() 
						{
							return vmAjax;
						});
					});
	});
}

function VNCOnChange(index)
{
	document.getElementById("menuSelection").selectedIndex = 0;
	MenuOnChange(document.getElementById("menuSelection").selectedIndex);
	document.getElementById("monitorBG").src = vmInfo["noConnectionUrl"];
	$.when(GetVMState(index)).done(function() 
	{
		if(vmState == "8")
		{
			$.when(GetVNCState(index)).done(function() 
					{
						if (vncState == "Off")
						{
							SetVNCSource(index);
						}
						else if (vncState == "On")
						{
							vncAjax = $.get("Assets/php/getGuestIP.php",
							{
								id:vmInfo["vm"][index]["id"], 
								path:vmInfo["vm"][index]["path"], 
								username:vmInfo["vm"][index]["username"],
								password:vmInfo["vm"][index]["password"],
								vncPort:vmInfo["vm"][index]["vncPort"],
								vmAction:"0"
							}
							,function(result)
							{
								result = JSON.parse(result);
								console.log(result);

								var ip = result[2];
								var vncUrl = vmInfo["vncUrl"];
								vncUrl = vncUrl.replace(/ip/g, ip);

								console.log(vncUrl);
								document.getElementById("monitorBG").src = vncUrl;
							});
						}
						else
						{
							SetVNCSource(index);
						}
					});
		}
	});
}