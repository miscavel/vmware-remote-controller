# VMWare Remote Controller

Virtual Machine web-based controller via javascript, php and C++ VMWare API (API implementation can be found in source).

Virtual Machine paths can be configured in [vmInfo.json](https://gitlab.com/Miscavel/vmware-remote-controller/-/blob/master/Assets/js/vmInfo.json).

Preview:

![preview.png](screenshot/preview.png)

If the target VM has noVNC installed and VNC port setup in vmInfo.json, the monitor screen in the middle would automatically connect and display the VM's current screen.

Credits: Miscavel & Nemeria