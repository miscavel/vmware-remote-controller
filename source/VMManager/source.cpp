#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <IPHlpApi.h>

#include "header/vix.h"

#define USE_WORKSTATION

#ifdef USE_WORKSTATION

#define  CONNTYPE    VIX_SERVICEPROVIDER_VMWARE_WORKSTATION

#define  HOSTNAME ""
#define  HOSTPORT 0
#define  USERNAME ""
#define  PASSWORD ""

#define  VMPOWEROPTIONS   VIX_VMPOWEROP_LAUNCH_GUI   // Launches the VMware Workstaion UI
                                                     // when powering on the virtual machine.

#define VMXPATH_INFO "where vmxpath is an absolute path to the .vmx file " \
                     "for the virtual machine."

#else    // USE_WORKSTATION

/*
 * For VMware Server 2.0
 */

#define CONNTYPE VIX_SERVICEPROVIDER_VMWARE_VI_SERVER

#define HOSTNAME ""
/*
 * NOTE: HOSTPORT is ignored, so the port should be specified as part
 * of the URL.
 */
#define HOSTPORT 0
#define USERNAME ""
#define PASSWORD ""

#define  VMPOWEROPTIONS VIX_VMPOWEROP_NORMAL

#define VMXPATH_INFO "where vmxpath is a datastore-relative path to the " \
                     ".vmx file for the virtual machine, such as "        \
                     "\"[standard] ubuntu/ubuntu.vmx\"."

#endif    // USE_WORKSTATION

/*
 * Global variables.
 */

static char *progName;


/*
 * Local functions.
 */

char* GetHostIP(char adapterName[])
{
	IP_ADAPTER_INFO  *pAdapterInfo;
	ULONG            ulOutBufLen;
	DWORD            dwRetVal;

	pAdapterInfo = (IP_ADAPTER_INFO *) malloc( sizeof(IP_ADAPTER_INFO) );
	ulOutBufLen = sizeof(IP_ADAPTER_INFO);

	if (GetAdaptersInfo( pAdapterInfo, &ulOutBufLen) != ERROR_SUCCESS) 
	{
		free (pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO *) malloc ( ulOutBufLen );
	}

	if ((dwRetVal = GetAdaptersInfo( pAdapterInfo, &ulOutBufLen)) != ERROR_SUCCESS) 
	{
		printf("GetAdaptersInfo call failed with %d\n", dwRetVal);
	}

	PIP_ADAPTER_INFO pAdapter = pAdapterInfo;
	while (pAdapter) 
	{
		if (strcmp(pAdapter->Description, adapterName) == 0)
		{
			return pAdapter->IpAddressList.IpAddress.String;
		}
		/*printf("Adapter Name: %s\n", pAdapter->AdapterName);
		printf("Adapter Desc: %s\n", pAdapter->Description);
		printf("\tAdapter Addr: \t");
		for (UINT i = 0; i < pAdapter->AddressLength; i++) 
		{
			if (i == (pAdapter->AddressLength - 1))
				printf("%.2X\n",(int)pAdapter->Address[i]);
			else
				printf("%.2X-",(int)pAdapter->Address[i]);
		}
		printf("IP Address: %s\n", pAdapter->IpAddressList.IpAddress.String);
		printf("IP Mask: %s\n", pAdapter->IpAddressList.IpMask.String);
		printf("\tGateway: \t%s\n", pAdapter->GatewayList.IpAddress.String);
		printf("\t***\n");
		if (pAdapter->DhcpEnabled) 
		{
			printf("\tDHCP Enabled: Yes\n");
			printf("\t\tDHCP Server: \t%s\n", pAdapter->DhcpServer.IpAddress.String);
		}
		else
			printf("\tDHCP Enabled: No\n");*/
		pAdapter = pAdapter->Next;
	}
	if (pAdapterInfo)
        free(pAdapterInfo);
}

VixError InitializeConnection(VixHandle &jobHandle, VixHandle &hostHandle)
{
	jobHandle = VixHost_Connect(VIX_API_VERSION,
                                CONNTYPE,
                                HOSTNAME, // *hostName,
                                HOSTPORT, // hostPort,
                                USERNAME, // *userName,
                                PASSWORD, // *password,
                                0, // options,
                                VIX_INVALID_HANDLE, // propertyListHandle,
                                NULL, // *callbackProc,
                                NULL); // *clientData);

    VixError err = VixJob_Wait(jobHandle, 
							   VIX_PROPERTY_JOB_RESULT_HANDLE, 
                               &hostHandle,
                               VIX_PROPERTY_NONE);
    return err;
}

VixError Login(VixHandle &jobHandle, VixHandle &vmHandle, char *username, char *password, int option = 0)
{
	jobHandle = VixVM_LoginInGuest(vmHandle,
                   username,
                   password,
                   option,
                   NULL,
                   NULL);
	VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);

	return err;
}

VixError OpenVM(VixHandle &jobHandle, VixHandle &hostHandle, VixHandle &vmHandle, char* vmxPath)
{
	jobHandle = VixVM_Open(hostHandle,
                           vmxPath,
                           NULL, // VixEventProc *callbackProc,
                           NULL); // void *clientData);
    VixError err = VixJob_Wait(jobHandle, 
							   VIX_PROPERTY_JOB_RESULT_HANDLE, 
						       &vmHandle,
                               VIX_PROPERTY_NONE);
    return err;
}

VixError PowerOff(VixHandle &jobHandle, VixHandle &vmHandle)
{
	jobHandle = VixVM_PowerOff(vmHandle,
                               VIX_VMPOWEROP_NORMAL,
                               NULL, // *callbackProc,
                               NULL); // *clientData);
    
	VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    
	return err;
}

VixError PowerOn(VixHandle &jobHandle, VixHandle &vmHandle)
{
	jobHandle = VixVM_PowerOn(vmHandle,
                              VMPOWEROPTIONS,
                              VIX_INVALID_HANDLE,
                              NULL, // *callbackProc,
                              NULL); // *clientData);
    VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
    
	return err;
}

VixError RunProgram(VixHandle &jobHandle, VixHandle &vmHandle, char *appPath, char *appName)
{
	strcat(appPath,appName);

	jobHandle = VixVM_RunProgramInGuest(vmHandle,
                                    appPath,
                                    "",
                                    VIX_RUNPROGRAM_RETURN_IMMEDIATELY,  // options,
                                    VIX_INVALID_HANDLE, // propertyListHandle,
                                    NULL, // callbackProc,
                                    NULL); // clientData
	VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
	
	return err;
}

VixError RunScriptFromGuest(VixHandle &jobHandle, VixHandle &vmHandle, char *script, bool wait, char *interpreter="\/bin\/bash")
{
	jobHandle = VixVM_RunScriptInGuest(vmHandle,
                                   interpreter,
                                   script,
                                   0, // options,
                                   VIX_INVALID_HANDLE, // propertyListHandle,
                                   NULL, // callbackProc,
                                   NULL); // clientData

	VixError err = VIX_OK;
	if (wait)
	{
		VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
	}
	else
	{
		Sleep(1000);
	}
	return err;
}

VixError Screenshot(VixHandle &jobHandle, VixHandle &vmHandle, std::size_t &byte_count, int *&screen_bits)
{
	jobHandle = VixVM_CaptureScreenImage(vmHandle,
                               VIX_CAPTURESCREENFORMAT_PNG,
                               VIX_INVALID_HANDLE,
                               NULL,
                               NULL);
	VixError err = VixJob_Wait(jobHandle,
					  VIX_PROPERTY_JOB_RESULT_SCREEN_IMAGE_DATA,
					  &byte_count, &screen_bits,
					  VIX_PROPERTY_NONE);

	return err;
}

VixError SnapshotCreate(VixHandle &jobHandle, VixHandle &vmHandle, VixHandle &snapshotHandle, char snapshotName[], char snapshotDescription[])
{
	jobHandle = VixVM_CreateSnapshot(vmHandle,
                                        snapshotName,
                                        snapshotDescription,
                                        VIX_SNAPSHOT_INCLUDE_MEMORY,
                                        VIX_INVALID_HANDLE,
                                        NULL, // *callbackProc,
                                        NULL); // *clientData);
	VixError err = VixJob_Wait(jobHandle, 
								VIX_PROPERTY_JOB_RESULT_HANDLE,
								&snapshotHandle,
								VIX_PROPERTY_NONE);
	return err;
}

VixError ViewAllSnapshots(VixHandle &snapshotHandle, VixHandle &vmHandle)
{
	std::queue<VixHandle> listQueue;
	char *snapshotName;
	int numChildren;
	VixError err = VixVM_GetRootSnapshot(vmHandle, 0, &snapshotHandle);
	listQueue.push(snapshotHandle);

	while(!listQueue.empty())
	{
		err = Vix_GetProperties(listQueue.front(), VIX_PROPERTY_SNAPSHOT_DISPLAYNAME, &snapshotName,VIX_PROPERTY_NONE);
		printf("%s\n",snapshotName);
		err = VixSnapshot_GetNumChildren(listQueue.front(), &numChildren);
		for(int i = 0; i < numChildren; i++)
		{
			err = VixSnapshot_GetChild(listQueue.front(),i,&snapshotHandle);
			listQueue.push(snapshotHandle);
		}
		listQueue.pop();
	}
	return err;
}

VixError GetSnapshotDescription(VixHandle &snapshotHandle, VixHandle &vmHandle, int queueID)
{
	std::queue<VixHandle> listQueue;
	char *snapshotName;
	int numChildren;
	VixError err = VixVM_GetRootSnapshot(vmHandle, 0, &snapshotHandle);
	listQueue.push(snapshotHandle);
	int index = 0;
	while (!listQueue.empty())
	{
		err = Vix_GetProperties(listQueue.front(), VIX_PROPERTY_SNAPSHOT_DESCRIPTION, &snapshotName, VIX_PROPERTY_NONE);
		if (index == queueID)
		{
			printf("%s\n",snapshotName);
			return err;
		}
		err = VixSnapshot_GetNumChildren(listQueue.front(), &numChildren);
		for (int i = 0; i < numChildren; i++)
		{
			err = VixSnapshot_GetChild(listQueue.front(), i, &snapshotHandle);
			listQueue.push(snapshotHandle);
		}
		listQueue.pop();
		index++;
	}
	return err;
}

VixError SnapshotRestore(VixHandle &jobHandle, VixHandle &vmHandle, VixHandle &snapshotHandle)
{
	jobHandle = VixVM_RevertToSnapshot(vmHandle,
		snapshotHandle,
		VMPOWEROPTIONS, // options,
		VIX_INVALID_HANDLE,
		NULL, // *callbackProc,
		NULL); // *clientData);
	VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
	return err;
}

VixError RestoreByQueueID(VixHandle &jobHandle, VixHandle &vmHandle, VixHandle &snapshotHandle, int queueID)
{
	std::queue<VixHandle> listQueue;
	char *snapshotName;
	int numChildren;
	VixError err = VixVM_GetRootSnapshot(vmHandle, 0, &snapshotHandle);
	listQueue.push(snapshotHandle);
	int index = 0;
	while (!listQueue.empty())
	{
		err = Vix_GetProperties(listQueue.front(), VIX_PROPERTY_SNAPSHOT_DISPLAYNAME, &snapshotName, VIX_PROPERTY_NONE);
		if (index == queueID)
		{
			return SnapshotRestore(jobHandle, vmHandle, listQueue.front());
		}
		err = VixSnapshot_GetNumChildren(listQueue.front(), &numChildren);
		for (int i = 0; i < numChildren; i++)
		{
			err = VixSnapshot_GetChild(listQueue.front(), i, &snapshotHandle);
			listQueue.push(snapshotHandle);
		}
		listQueue.pop();
		index++;
	}
	return err;
}

VixError WaitForTools(VixHandle &jobHandle, VixHandle &vmHandle)
{
	jobHandle = VixVM_WaitForToolsInGuest(vmHandle,
                                      300, // timeoutInSeconds
                                      NULL, // callbackProc
                                      NULL); // clientData

	VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);

	return err;
}

//Main Necessity
void InitializeVM(VixHandle &jobHandle, VixHandle &vmHandle, VixHandle &hostHandle, char *vmxPath)
{
	printf("%s\n", Vix_GetErrorText(InitializeConnection(jobHandle, hostHandle), NULL));
	Vix_ReleaseHandle(jobHandle);

	printf("%s\n", OpenVM(jobHandle, hostHandle, vmHandle, vmxPath));
    Vix_ReleaseHandle(jobHandle);
}

void LoginToVM(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[])
{
	error = WaitForTools(jobHandle, vmHandle);
	Vix_ReleaseHandle(jobHandle);

	error = Login(jobHandle, vmHandle, username, password, VIX_LOGIN_IN_GUEST_REQUIRE_INTERACTIVE_ENVIRONMENT);
	Vix_ReleaseHandle(jobHandle);
}

VixError CopyGuestToHost(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[], char srcPath[], char srcFile[], char destPath[], char destFile[])
{
	LoginToVM(jobHandle, vmHandle, error, username, password); 

	strcat(srcPath,srcFile);

	strcat(destPath,destFile);
	
	//printf("%s\n%s\n", srcPath, destPath);
	jobHandle = VixVM_CopyFileFromGuestToHost(vmHandle,
										  srcPath,  // src name
                                          destPath, // dest name
                                          0, // options
                                          VIX_INVALID_HANDLE, // propertyListHandle
                                          NULL, // callbackProc
                                          NULL); // clientData
	VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
	return err;
}

VixError CopyHostToGuest(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[], char srcPath[], char srcFile[], char destPath[], char destFile[])
{
	LoginToVM(jobHandle, vmHandle, error, username, password); 

	strcat(srcPath,srcFile);

	strcat(destPath,destFile);
	
	//printf("%s\n%s\n", srcPath, destPath);
	jobHandle = VixVM_CopyFileFromHostToGuest(vmHandle,
										  srcPath,  // src name
                                          destPath, // dest name
                                          0, // options
                                          VIX_INVALID_HANDLE, // propertyListHandle
                                          NULL, // callbackProc
                                          NULL); // clientData
	VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);
	return err;
}

//Main Function
void CheckVNCState(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[], char guestID[], char hostPath[])
{
	LoginToVM(jobHandle, vmHandle, error, username, password); 

	char cmd[200] = "echo $HOSTNAME > Documents\/";
	strcat(cmd, guestID);
	strcat(cmd, ".txt | echo $(lsof -t -i:6080 | wc -l) >> Documents\/");
	strcat(cmd, guestID);
	strcat(cmd, ".txt");
	error = RunScriptFromGuest(jobHandle, vmHandle, cmd, true);
	Vix_ReleaseHandle(jobHandle);

	strcpy(cmd, guestID);
	strcat(cmd, ".txt");

	char destPath[200] = "\/home\/";
	strcat(destPath, username);
	strcat(destPath, "\/Documents\/");
	error = CopyGuestToHost(jobHandle, vmHandle, error, username, password, destPath, cmd, hostPath, cmd);

	if (!VIX_FAILED(error))
	{
		std::string guestIP, vncInstance;
		std::ifstream guestFile(hostPath, std::ifstream::in);
		std::getline(guestFile, guestIP);
		std::getline(guestFile, vncInstance);
		std::cout << vncInstance << "\n";
		if (vncInstance == "0")
		{
			printf("Off\n");
		}
		else
		{
			printf("On\n");
		}
	}
	else
	{
		printf("Failed\n");
	}
	printf("%s\n", Vix_GetErrorText(error, NULL));
}

void GetGuestIP(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[], char guestID[], char hostPath[])
{
	LoginToVM(jobHandle, vmHandle, error, username, password); 

	char cmd[200] = "echo $HOSTNAME > Documents\/";
	strcat(cmd, guestID);
	strcat(cmd, ".txt");
	error = RunScriptFromGuest(jobHandle, vmHandle, cmd, true);
	Vix_ReleaseHandle(jobHandle);

	strcpy(cmd, guestID);
	strcat(cmd, ".txt");

	char destPath[200] = "\/home\/";
	strcat(destPath, username);
	strcat(destPath, "\/Documents\/");
	error = CopyGuestToHost(jobHandle, vmHandle, error, username, password, destPath, cmd, hostPath, cmd);

	std::string guestIP;
	std::ifstream guestFile(hostPath, std::ifstream::in);
	std::getline(guestFile, guestIP);
	std::cout << guestIP << "\n";
}

void GetVMPowerState(VixHandle &vmHandle, VixError &error, char guestID[])
{
	VixPowerState vmPowerState;
	error = Vix_GetProperties(vmHandle,
				VIX_PROPERTY_VM_POWER_STATE,
				&vmPowerState,
				VIX_PROPERTY_NONE);
	printf("%s Get VM State\n", guestID);
	printf("%d\n", vmPowerState);
		
	if(vmPowerState == 1)
	{
		printf("Powering Off\n");
	}
	else if(vmPowerState == 2 || vmPowerState == 32)
	{
		printf("Powered Off / Suspended\n");
	}
	else if(vmPowerState == 4)
	{
		printf("Powering On\n");
	}
	else if(vmPowerState == 8)
	{
		printf("Powered On\n");
	}
	else if(vmPowerState == 10)
	{
		printf("Suspending\n");
	}
}

void ReleaseVM(VixHandle &vmHandle, VixHandle &hostHandle)
{
	Vix_ReleaseHandle(vmHandle);
	VixHost_Disconnect(hostHandle);
}

void SetGuestHostnameToIP(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[])
{
	LoginToVM(jobHandle, vmHandle, error, username, password); 

	char cmd[200] = "sudo hostname `/sbin/ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}'`";
	error = RunScriptFromGuest(jobHandle, vmHandle, cmd, true);
	Vix_ReleaseHandle(jobHandle);
}

void StartVNC(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[], char port[])
{
	char hostIP[100];
	strcpy(hostIP, GetHostIP("Qualcomm Atheros AR956x Wireless Network Adapter"));
	//printf("%s\n", hostIP);

	LoginToVM(jobHandle, vmHandle, error, username, password);

	char cmd[200];
	strcpy(cmd, "bash Documents/noVNC-1.1.0/utils/launch.sh --vnc ");
	strcat(cmd, hostIP);
	strcat(cmd, ":");
	error = RunScriptFromGuest(jobHandle, vmHandle, strcat(cmd, port), false);
	Vix_ReleaseHandle(jobHandle);
}

void StopVNC(VixHandle &jobHandle, VixHandle &vmHandle, VixError &error, char username[], char password[])
{
	LoginToVM(jobHandle, vmHandle, error, username, password);

	char cmd[200];
	strcpy(cmd, "kill -9 $(lsof -t -i:6080)");
	error = RunScriptFromGuest(jobHandle, vmHandle, cmd, true);
	Vix_ReleaseHandle(jobHandle);
}

VixError Suspend(VixHandle &jobHandle, VixHandle &vmHandle)
{
	jobHandle = VixVM_Suspend(vmHandle,
                              VIX_VMPOWEROP_NORMAL,
                              NULL,
                              NULL);
    VixError err = VixJob_Wait(jobHandle, VIX_PROPERTY_NONE);

	return err;
}

int main(int argc, char **argv)
{
	VixError error;
	char actionName[100];
	char actionPath[100];
	char destFile[100];
	char destPath[100];
	char input[200] = "C:\\Users\\Miscavel\\Documents\\Virtual Machines\\Ubuntu 14.04.6 64-bit\\Ubuntu 14.04.6 64-bit.vmx";
	char guestID[200] = "Guest1";
	char guestDir[200] = "Guest\\";
	char password[100] = "ubuntu";
	char port[100] = "5902";
	char snapshotDesc[100];
	char snapshotIndex[100];
	char snapshotName[100];
	char srcFile[100];
	char srcPath[100];
	char username[100] = "ubuntu";
	char vmAction[100] = "0";
	char *vmxPath = input;

	strcpy(guestID, argv[1]);
	strcpy(vmxPath, argv[2]);
	strcpy(username, argv[3]);
	strcpy(password, argv[4]);
	strcpy(port, argv[5]);
	strcpy(vmAction, argv[6]);

	VixHandle hostHandle = VIX_INVALID_HANDLE;
    VixHandle jobHandle = VIX_INVALID_HANDLE;
	VixHandle snapshotHandle = VIX_INVALID_HANDLE;
    VixHandle vmHandle = VIX_INVALID_HANDLE;

	InitializeVM(jobHandle, vmHandle, hostHandle, vmxPath);

	if (strcmp(vmAction, "0") == 0)
	{
		//Set & Get Guest Hostname
		SetGuestHostnameToIP(jobHandle, vmHandle, error, username, password);
		GetGuestIP(jobHandle, vmHandle, error, username, password, guestID, guestDir);
	}
	else if (strcmp(vmAction, "1") == 0)
	{
		//Start VNC
		printf("%s VNC Start\n", guestID);
		StartVNC(jobHandle, vmHandle, error, username, password, port);
	}
	else if (strcmp(vmAction, "2") == 0)
	{
		//Stop VNC
		printf("%s VNC Stop\n", guestID);
		StopVNC(jobHandle, vmHandle, error, username, password);
	}
	else if (strcmp(vmAction, "3") == 0)
	{
		//Start VM
		printf("%s Start VM\n", guestID);
		error = PowerOn(jobHandle, vmHandle);
		printf("%s\n", Vix_GetErrorText(error, NULL));
	}
	else if (strcmp(vmAction, "4") == 0)
	{
		//Suspend VM
		printf("%s Suspend VM\n", guestID);
		error = Suspend(jobHandle, vmHandle);
		printf("%s\n", Vix_GetErrorText(error, NULL));
	}
	else if (strcmp(vmAction, "5") == 0)
	{
		//Power Off
		printf("%s Shutdown VM\n", guestID);
		error = PowerOff(jobHandle, vmHandle);
		printf("%s\n", Vix_GetErrorText(error, NULL));
	}
	else if (strcmp(vmAction, "6") == 0)
	{
		//Get VM Power State
		GetVMPowerState(vmHandle, error, guestID);
	}
	else if(strcmp(vmAction, "7") == 0)
	{
		//Check VNC State
		CheckVNCState(jobHandle, vmHandle, error, username, password, guestID, guestDir);
	}
	else if(strcmp(vmAction, "8") == 0)
	{
		//Run Program in Guest
		strcpy(actionPath, argv[7]);
		strcpy(actionName, argv[8]);
		LoginToVM(jobHandle, vmHandle, error, username, password);
		RunProgram(jobHandle, vmHandle, actionPath, actionName);
		if (!VIX_FAILED(error))
		{
			printf("%s Running Program %s\n", guestID, actionPath);
		}
		else
		{
			printf("%s\n", Vix_GetErrorText(error, NULL));
		}
	}
	else if(strcmp(vmAction, "9") == 0)
	{
		//Run Script in Guest
		strcpy(actionPath, argv[7]);
		strcpy(actionName, argv[8]);
		LoginToVM(jobHandle, vmHandle, error, username, password);
		RunScriptFromGuest(jobHandle, vmHandle, actionName, false, actionPath);
		if (!VIX_FAILED(error))
		{
			printf("%s Running Script %s %s\n", guestID, actionPath, actionName);
		}
		else
		{
			printf("%s\n", Vix_GetErrorText(error, NULL));
		}
	}
	else if(strcmp(vmAction, "10") == 0)
	{
		//Copy Host to Guest
		strcpy(srcPath, argv[7]);
		strcpy(srcFile, argv[8]);
		strcpy(destPath, "\/home\/");
		strcat(destPath, username);
		strcat(destPath, argv[9]);
		strcpy(destFile, argv[10]);
		LoginToVM(jobHandle, vmHandle, error, username, password);
		CopyHostToGuest(jobHandle, vmHandle, error, username, password, srcPath, srcFile, destPath, destFile);
		if (!VIX_FAILED(error))
		{
			printf("%s Uploaded File %s to %s\n", guestID, srcPath, destPath);
		}
		else
		{
			printf("%s\n", Vix_GetErrorText(error, NULL));
		}
	}
	else if(strcmp(vmAction, "11") == 0)
	{
		//Copy Guest to Host
		strcpy(srcPath, "\/home\/");
		strcat(srcPath, username);
		strcat(srcPath, argv[7]);
		strcpy(srcFile, argv[8]);
		strcpy(destPath, argv[9]);
		strcpy(destFile, argv[10]);
		LoginToVM(jobHandle, vmHandle, error, username, password);
		CopyGuestToHost(jobHandle, vmHandle, error, username, password, srcPath, srcFile, destPath, destFile);
		if (!VIX_FAILED(error))
		{
			printf("%s Downloaded File %s\n", guestID, srcPath);
		}
		else
		{
			printf("%s\n", Vix_GetErrorText(error, NULL));
		}
	}
	else if(strcmp(vmAction, "12") == 0)
	{
		//Take Screenshot
		strcpy(actionPath, argv[7]);
		strcpy(actionName, argv[8]);
		LoginToVM(jobHandle, vmHandle, error, username, password);
		if(!VIX_FAILED(error))
		{
			int *screen_bits = new int;
			std::size_t byte_count = 0;

			error = Screenshot(jobHandle, vmHandle, byte_count, screen_bits);
			if (!VIX_FAILED(error))
			{
				char ssName[500];
				
				strcpy(ssName, actionPath);
				strcat(ssName, actionName);
				FILE *fp = fopen(ssName, "wb+");
				if (fp) {
					fwrite(screen_bits, byte_count, 1, fp);
					fclose(fp);
				}
				// Free blob memory when done.
				Vix_FreeBuffer(screen_bits);
			}

			printf("%s Taken a ScreenShot\n", guestID);
		}
		else
		{
			printf("%s\n", Vix_GetErrorText(error, NULL));
		}
	}
	else if(strcmp(vmAction, "13") == 0)
	{
		//Pull Snapshots
		ViewAllSnapshots(snapshotHandle, vmHandle);
	}
	else if(strcmp(vmAction, "14") == 0)
	{
		//Pull Snapshots Description
		strcpy(snapshotIndex, argv[7]);
		GetSnapshotDescription(snapshotHandle, vmHandle, atoi(snapshotIndex));
	}
	else if(strcmp(vmAction, "15") == 0)
	{
		//Create Snapshot
		strcpy(snapshotName, argv[7]);
		strcpy(snapshotDesc, argv[8]);
		error = SnapshotCreate(jobHandle, vmHandle, snapshotHandle, snapshotName, snapshotDesc);
		if (!VIX_FAILED(error))
		{
			printf("%s Created Snapshot %s : %s\n", guestID, snapshotName, snapshotDesc);
		}
		else
		{
			printf("%s\n", Vix_GetErrorText(error, NULL));
		}
	}
	else if(strcmp(vmAction, "16") == 0)
	{
		//Revert Snapshot
		strcpy(snapshotIndex, argv[7]);
		error = RestoreByQueueID(jobHandle, vmHandle, snapshotHandle, atoi(snapshotIndex));
		if (!VIX_FAILED(error))
		{
			printf("%s Restored Snapshot\n", guestID);
		}
		else
		{
			printf("%s\n", Vix_GetErrorText(error, NULL));
		}
	}

	ReleaseVM(vmHandle, hostHandle);
}